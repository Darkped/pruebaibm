﻿using System;
using System.Collections.Generic;

namespace TiendaApi.Database.Models
{
    public partial class Usuarios
    {
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Password { get; set; }
        public string Usuario { get; set; }
    }
}
