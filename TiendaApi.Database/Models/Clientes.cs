﻿using System;
using System.Collections.Generic;

namespace TiendaApi.Database.Models
{
    public partial class Clientes
    {
        public int IdCliente { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public int? Telefono { get; set; }
        public string Direccion { get; set; }
    }
}
