﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TiendaApi.Model;

namespace TiendaApi.Interfaces.Clientes
{
    public interface IClientes
    {
        Task<Database.Models.Clientes> ObtenerCliente(int idCliente);
        Task<Database.Models.Clientes> ObtenerClienteSp(int idCliente);
        Task<RequestResult> CrearCliente(Database.Models.Clientes objCliente);
        Task<RequestResult> CrearClienteSp(Database.Models.Clientes objCliente);
        Task<RequestResult> ActualizarCliente(Database.Models.Clientes objCliente);
        Task<RequestResult> ActualizarClienteSp(Database.Models.Clientes objCliente);
        Task<RequestResult> EliminarPaciente(int idCliente);
        Task<RequestResult> EliminarPacienteSp(int idCliente);
        Task<List<Database.Models.Clientes>> ListadoCliente();
        Task<List<Database.Models.Clientes>> ListadoClienteSp();
    }
}
