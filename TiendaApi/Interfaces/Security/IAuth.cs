﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TiendaApi.Model;

namespace TiendaApi.Interfaces.Security
{
  
    public interface IAuth
    {
        Task<UserInfo> Authorize(string username, string password);
        string GetHashSha5(string text);
        Task<RequestResult> CreateUser(Database.Models.Usuarios usuario);


    }
}
