﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using TiendaApi.Interfaces.Security;
using TiendaApi.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TiendaApi.Controllers.Security
{
    [ApiController]
    [EnableCors("MyPolicy")]
    [Route("api/auth")]
    [Consumes("application/json")]
    public class AuthController : ControllerBase
    {
        private IAuth _Auth { get; }
        private IConfiguration _configuration {get;}

        public AuthController(IAuth auth, IConfiguration configuration)
        {
            _Auth = auth;
            _configuration = configuration;
        }

        /// <summary>
        /// Login Usuarios
        /// </summary>
        /// <param name="objUser"></param>
        /// <returns></returns>
        
        [HttpPost("[action]")]
        public async Task<ActionResult<UserInfo>> Login(User objUser)
        {
            var user = await _Auth.Authorize(objUser.username, objUser.password);
            if(user != null)
            {

                var claims = new[]
             {
                new Claim(JwtRegisteredClaimNames.UniqueName,user.usuario),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("username", user.usuario),                
                new Claim("userid", user.id.ToString())                
            };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SecureKey"]));
                var expireInMinutes = DateTime.UtcNow.AddMinutes(3000);
                var token = new JwtSecurityToken(
                  audience: "tienda.com",
                  issuer: "tienda.com",
                  claims: claims,
                  expires: expireInMinutes,
                  signingCredentials: new SigningCredentials(key,SecurityAlgorithms.HmacSha256));
                user.token = new JwtSecurityTokenHandler().WriteToken(token);
                user.expiration = expireInMinutes;
                return Ok(user);

            }
            else
            {
                return Unauthorized();
            }
            
        }
        /// <summary>
        /// Registrar un nuevo usuario
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPost("Crear")]
        public async Task<ActionResult<RequestResult>> CreateUser(Database.Models.Usuarios usuario)
        {
            if (ModelState.IsValid)
            {
                var user = await _Auth.CreateUser(usuario);
                if(user != null)
                {
                    return Ok(user);
                }
                return BadRequest("Error con el servidor intenelo mas tarde");
            }
            return BadRequest("Modelo invalido");
        }
    }
}
