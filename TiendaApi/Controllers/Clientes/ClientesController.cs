﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using TiendaApi.Database.Models;
using TiendaApi.Interfaces.Clientes;
using TiendaApi.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TiendaApi.Controllers.Clientes
{
    [Route("api/clientes")]
    [EnableCors("MyPolicy")]
    [ApiController]
    [Consumes("application/json")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ClientesController : ControllerBase
    {
        private IClientes _Clientes { get; }
        public ClientesController(IClientes clientes)
        {
            _Clientes = clientes;
        }
        /// <summary>
        /// Insertar un nuevo cliente
        /// </summary>
        /// <param name="cliente"> Datos del cliente a crear</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<RequestResult>> Insertar(Database.Models.Clientes cliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _Clientes.CrearCliente(cliente);
                    if (result.Registros > 0)
                    {
                        return Ok(result);

                    }
                    else
                    {
                        return result;
                    }
                }
                return StatusCode(401);

            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }
        /// <summary>
        /// Insertar un nuevo cliente por un prcedimiento almacenado
        /// </summary>
        /// <param name="cliente"> Datos del cliente a crear</param>
        /// <returns></returns>
        [HttpPost("InsertarClienteSp")]
        public async Task<ActionResult<RequestResult>> InsertarSp(Database.Models.Clientes cliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _Clientes.CrearClienteSp(cliente);
                    if (result.Registros > 0)
                    {
                        return Ok(result);

                    }
                    else
                    {
                        return result;
                    }
                }
                return StatusCode(401);

            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Obtener Cliente por su id
        /// </summary>
        /// <param name="idCliente">id del cliente a buscar</param>
        /// <returns></returns>
        [HttpGet("{idCliente}")]
        async public Task<ActionResult<Database.Models.Clientes>> ObtenerCliente(int idCliente)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var result = await _Clientes.ObtenerCliente(idCliente);
                    if (result != null)
                    {
                        return Ok(result);

                    }
                }
                return Ok(false);

            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }
        /// <summary>
        /// Obtener CLiente por su id por un procedimiento almacenado
        /// </summary>
        /// <param name="idCliente">id del cliente a buscar</param>
        /// <returns></returns>
        [HttpGet("ObtenerClienteSp/{idCliente}")]
        async public Task<ActionResult<Database.Models.Clientes>> ObtenerClienteSp(int idCliente)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var result = await _Clientes.ObtenerClienteSp(idCliente);
                    if (result != null)
                    {
                        return Ok(result);

                    }
                }
                return Ok(false);

            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }
        /// <summary>
        /// Listado de clientes
        /// </summary>      
        /// <returns></returns>
        [HttpGet("Listado")]
        async public Task<ActionResult<List<Database.Models.Clientes>>> ListadoCliente()
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var result = await _Clientes.ListadoCliente();
                    if (result != null)
                    {
                        return Ok(result);

                    }
                }
                return BadRequest("Modelo no valido");

            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }
        /// <summary>
        /// Listado de clientes por un procedimineto almacenado
        /// </summary>      
        /// <returns></returns>
        [HttpGet("ListadoSp")]
        async public Task<ActionResult<List<Database.Models.Clientes>>> ListadoClienteSp()
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var result = await _Clientes.ListadoClienteSp();
                    if (result != null)
                    {
                        return Ok(result);

                    }
                }
                return BadRequest("Modelo no valido");

            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Actualizar Cliente
        /// </summary>
        /// <param name="cliente"> Datos del cliente a actualizar</param>
        /// <returns></returns>
        [HttpPut("Actualizar")]
        async public Task<ActionResult<Database.Models.Clientes>> ActualizarCliente(Database.Models.Clientes cliente)
        {
            try
            {
                if (ModelState.IsValid)
                {




                    var result = await _Clientes.ActualizarCliente(cliente);
                    if (result != null)
                    {
                        return Ok(result);
                    }
                    else
                    {
                        return Unauthorized();
                    }
                }


                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }
        /// <summary>
        /// Actualizar Cliente por procedimiento almacenado
        /// </summary>
        /// <param name="cliente"> Datos del cliente a actualizar</param>
        /// <returns></returns>
        [HttpPut("ActualizarSp")]
        async public Task<ActionResult<Database.Models.Clientes>> ActualizarClienteSp(Database.Models.Clientes cliente)
        {
            try
            {
                if (ModelState.IsValid)
                {




                    var result = await _Clientes.ActualizarClienteSp(cliente);
                    if (result != null)
                    {
                        return Ok(result);
                    }
                    else
                    {
                        return Unauthorized();
                    }
                }


                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }
        /// <summary>
        /// Eliminar Cliente por su id
        /// </summary>
        /// <param name="idCliente">id del cliente a eliminar</param>
        /// <returns></returns>
        [HttpDelete("{idCliente}")]
        async public Task<ActionResult<RequestResult>> EliminarCliente(int idCliente)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var result = await _Clientes.EliminarPaciente(idCliente);
                    if (result.Registros == 1)
                    {
                        return Ok(result);

                    }
                }
                return Ok(false);

            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }
        /// <summary>
        /// Eliminar Cliente por su id usando un procedimiento almacenado
        /// </summary>
        /// <param name="idCliente">id del cliente a eliminar</param>
        /// <returns></returns>
        [HttpDelete("EliminarSp/{idCliente}")]
        async public Task<ActionResult<RequestResult>> EliminarClienteSp(int idCliente)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var result = await _Clientes.EliminarPacienteSp(idCliente);
                    if (result.Registros == 1)
                    {
                        return Ok(result);

                    }
                }
                return Ok(false);

            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }
    }
}



