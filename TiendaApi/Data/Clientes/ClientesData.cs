﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TiendaApi.Database.Models;
using TiendaApi.Interfaces.Clientes;
using TiendaApi.Model;

namespace TiendaApi.Data.Clientes
{
    public class ClientesData : IClientes
    {
        readonly TiendaContext db;
        public ClientesData(TiendaContext _db)
        {
            db = _db;
        }
        /// <summary>
        /// Actualizar un cliente
        /// </summary>
        /// <param name="objCliente">Datos del cliente</param>
        /// <returns></returns>
        public async Task<RequestResult> ActualizarCliente(Database.Models.Clientes objCliente)
        {
            RequestResult result = new RequestResult();
            var original = db.Clientes.Find(objCliente.IdCliente);

            if (original == null)
            {
                result.Registros = 0;
                result.Id = 0;
                result.Mensaje = "Cliente no existe";
                return result;
            }
            else
            {
                db.Entry(original).CurrentValues.SetValues(objCliente);
                var rest = db.SaveChanges();
                result.Registros = rest;
                result.Id = objCliente.IdCliente;
                result.Mensaje = "Cliente Actualizado Correctamente";
                return result;
            }
        }
        /// <summary>
        /// Actualizar cliente usando un procedimeinto almacenado
        /// </summary>
        /// <param name="objCliente">Datos del cliente a actualizar</param>
        /// <returns></returns>
        public async Task<RequestResult> ActualizarClienteSp(Database.Models.Clientes objCliente)
        {
            RequestResult result = new RequestResult();

            var original = db.Clientes.FromSql("EXEC dbo.ConsultaClientePorId @idCliente={0}", objCliente.IdCliente).FirstOrDefault();
            if (original == null)
            {
                result.Registros = 0;
                result.Id = 0;
                result.Mensaje = "Cliente no existe";
                return result;
            }
            else
            {
                SqlParameter[] spParams = new SqlParameter[]
                {
                       new SqlParameter("@idCliente", SqlDbType.Int),
                       new SqlParameter("@primerNombre", SqlDbType.VarChar),
                       new SqlParameter("@segundoNombre",  SqlDbType.VarChar),
                       new SqlParameter("@primerApellido", SqlDbType.VarChar),
                       new SqlParameter("@segundoApellido", SqlDbType.VarChar),
                       new SqlParameter("@telefono", SqlDbType.Int),
                       new SqlParameter("@direccion", SqlDbType.VarChar),
                };
                spParams[0].Value = objCliente.IdCliente;
                spParams[1].Value = objCliente.PrimerNombre;
                spParams[2].Value = objCliente.SegundoNombre;
                spParams[3].Value = objCliente.PrimerApellido;
                spParams[4].Value = objCliente.SegundoApellido;
                spParams[5].Value = objCliente.Telefono;
                spParams[6].Value = objCliente.Direccion;
                db.Database.ExecuteSqlCommand("Execute ActualizarCliente @idCliente,@primerNombre,@segundoApellido,@primerApellido,@segundoApellido,@telefono, @direccion", spParams);
                result.Registros = 1;
                result.Id = (int)spParams[0].Value; ;
                result.Mensaje = "Cliente Actualizado Correctamente";
                return result;
            }



        }
        /// <summary>
        /// Crear un nuevo cliente
        /// </summary>
        /// <param name="objCliente">Datos del cliente</param>
        /// <returns></returns>
        public async Task<RequestResult> CrearCliente(Database.Models.Clientes objCliente)
        {
            RequestResult result = new RequestResult();
            await db.Clientes.AddAsync(objCliente);
            var rest = db.SaveChanges();
            result.Registros = rest;
            result.Id = objCliente.IdCliente;
            result.Mensaje = "Cliente creado correctamente";
            return result;

        }
        /// <summary>
        /// Crear un cliente usando procedimiento almacenado
        /// </summary>
        /// <param name="objCliente"></param>
        /// <returns></returns>
        public async Task<RequestResult> CrearClienteSp(Database.Models.Clientes objCliente)
        {
            RequestResult result = new RequestResult();
            SqlParameter[] spParams = new SqlParameter[]
             {
                       new SqlParameter("@idCliente", ""),
                       new SqlParameter("@primerNombre", SqlDbType.VarChar),
                       new SqlParameter("@segundoNombre",  SqlDbType.VarChar),
                       new SqlParameter("@primerApellido", SqlDbType.VarChar),
                       new SqlParameter("@segundoApellido", SqlDbType.VarChar),
                       new SqlParameter("@telefono", SqlDbType.Int),
                       new SqlParameter("@direccion", SqlDbType.VarChar),
            };
            spParams[0].SqlDbType = SqlDbType.Int;
            spParams[0].Direction = ParameterDirection.Output;
            spParams[1].Value = objCliente.PrimerNombre;
            spParams[2].Value = objCliente.SegundoNombre;
            spParams[3].Value = objCliente.PrimerApellido;
            spParams[4].Value = objCliente.SegundoApellido;
            spParams[5].Value = objCliente.Telefono;
            spParams[6].Value = objCliente.Direccion;
            db.Database.ExecuteSqlCommand("Execute InsertarCliente @idCliente OUT ,@primerNombre,@segundoApellido,@primerApellido,@segundoApellido,@telefono, @direccion", spParams);
            result.Id = (int)spParams[0].Value;
            result.Registros = 1;
            result.Mensaje = "Cliente creado correctamente";
            return result;
        }
        /// <summary>
        /// Eliminar cliente
        /// </summary>
        /// <param name="idCliente"> id del cliente a eliminar</param>
        /// <returns></returns>
        public async Task<RequestResult> EliminarPaciente(int idCliente)
        {
            var result = new RequestResult();
            var resultFull = db.Clientes.Find(idCliente);

            if (resultFull == null)
            {
                result.Registros = 0;
                result.Id = 0;
                result.Mensaje = "Cliente no existe";
                return result;
            }
            else
            {
                db.Clientes.Remove(resultFull);
                result.Registros = db.SaveChanges();
                result.Mensaje = "Cliente eliminado correctamente";
                result.Id = resultFull.IdCliente;
                return result;
            }
        }
        /// <summary>
        /// Eliminar Cliente con procedimiento almacenado
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public async Task<RequestResult> EliminarPacienteSp(int idCliente)
        {
            var result = new RequestResult();         
            var original = db.Clientes.FromSql("EXEC dbo.ConsultaClientePorId @idCliente={0}", idCliente).FirstOrDefault();
            if (original == null)
            {
                result.Registros = 0;
                result.Id = 0;
                result.Mensaje = "Cliente no existe";
                return result;
            }
            else
            {
                SqlParameter[] spParams = new SqlParameter[]
                {
                       new SqlParameter("@idCliente", SqlDbType.Int),

                };
                spParams[0].Value = idCliente;
                db.Database.ExecuteSqlCommand("Execute EliminarCliente @idCliente", spParams);
                result.Id = idCliente;
                result.Registros = 1;
                result.Mensaje = "Cliente eliminado correctamente";
                return result;
            }

        }
        public async Task<Database.Models.Clientes> ObtenerCliente(int idCliente)
        {
            var resultFull = db.Clientes.Find(idCliente);
            return resultFull;


        }
        public async Task<Database.Models.Clientes> ObtenerClienteSp(int idCliente)
        {
            var dbResults = db.Clientes.FromSql("EXEC dbo.ConsultaClientePorId @idCliente={0}", idCliente).FirstOrDefault();
            return dbResults;
        }
        public async Task<List<Database.Models.Clientes>> ListadoCliente()
        {
            var resultFull = db.Clientes.ToList();
            return resultFull;
        }
        public async Task<List<Database.Models.Clientes>> ListadoClienteSp()
        {
            var resultFull = db.Clientes.FromSql("EXEC dbo.ConsultarClientes").ToList();
            return resultFull;
        }
    }
}
