﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TiendaApi.Interfaces.Security;
using TiendaApi.Model;
using System.Security.Cryptography;
using System.Text;
using TiendaApi.Database.Models;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace TiendaApi.Data.Security
{
    public class Auth : IAuth
    {
        private readonly TiendaContext db;
      
        public Auth(TiendaContext dbcontext)
        {
            db = dbcontext;
           
        }


        public async Task<RequestResult> CreateUser(Usuarios objUsuario)
        {
            var Request = new RequestResult();          

            var result = db.Usuarios.Where(s => s.Usuario == objUsuario.Usuario).FirstOrDefault();
            if(result == null)
            {
                objUsuario.Password = GetHashSha5(objUsuario.Password);
                db.Usuarios.Add(objUsuario);
                Request.Registros = db.SaveChanges();
                Request.Mensaje = "Registro Completado Conrrectamente";
                Request.Id = objUsuario.IdUsuario;
            }
            else
            {
                Request.Registros = 0;
                Request.Mensaje = "Nombre de cuenta ya existe";
                Request.Id = objUsuario.IdUsuario;
            }
           
            return Request;


        }
        public async Task<UserInfo> Authorize(string user, string password)

        {
            var sha5 = GetHashSha5(password);
            var result = await (from u in db.Usuarios
                                where u.Usuario == user && u.Password == sha5
                                select new UserInfo
                                {
                                    id = u.IdUsuario,
                                    nombre = $"{u.Nombre} {u.Apellido}",
                                    usuario = u.Usuario                                   

                                }).FirstOrDefaultAsync();
            return result;
        }
        public string GetHashSha5(string text)
        {
            using (var objsha5 = SHA512.Create())
            {
                var hashedBytes = objsha5.ComputeHash(Encoding.UTF8.GetBytes(text));
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }

    }
}
