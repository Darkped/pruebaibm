﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TiendaApi.Model
{
    public class RequestResult
    {
        public int Id { get; set; }
        public int Registros { get; set; }
        public string Mensaje { get; set; }
    }
}
