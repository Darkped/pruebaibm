﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace TiendaApi.Validations
{
    public class UsuariosValidation : AbstractValidator<Database.Models.Usuarios>
    {
        public UsuariosValidation()
        {
            RuleFor(x => x.Nombre).NotEmpty();
            RuleFor(x => x.Apellido).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
            RuleFor(x => x.Usuario).NotEmpty();            
        }
    }
}
