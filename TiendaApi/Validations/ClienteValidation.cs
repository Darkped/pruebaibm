﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TiendaApi.Validations
{
    public class ClienteValidation : AbstractValidator<Database.Models.Clientes>
    {      
        public ClienteValidation()
        {

            RuleFor(x => x.PrimerApellido).NotEmpty();
            RuleFor(x => x.PrimerNombre).NotEmpty();
            RuleFor(x => x.Telefono).NotEmpty();
            RuleFor(x => x.Direccion).NotEmpty();
        }
    }
}
