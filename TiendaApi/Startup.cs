﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using TiendaApi.Data.Clientes;
using TiendaApi.Data.Security;
using TiendaApi.Database.Models;
using TiendaApi.Interfaces.Clientes;
using TiendaApi.Interfaces.Security;

namespace TiendaApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<TiendaContext>(option => option.UseSqlServer(Configuration.GetConnectionString("defaultConnection")));

            //Permisos
            //Seguidad
            services.AddTransient<IAuth, Auth>();
            //Clientes
            services.AddTransient<IClientes, ClientesData>();


            //origin
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            //Servicio de tokens
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
              .AddJwtBearer(options =>
              {
                  options.RequireHttpsMetadata = true;
                  options.SaveToken = true;
                  options.TokenValidationParameters = new TokenValidationParameters
                  {
                      ValidateIssuer = true,
                      ValidateAudience = true,
                      ValidateLifetime = true,
                      ValidateIssuerSigningKey = true,
                      ValidIssuer = "tienda.com",
                      ValidAudience = "tienda.com",
                      IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["SecureKey"])),//llave secreta
                      ClockSkew = TimeSpan.Zero
                  };
              });

            //origin
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials();
                       }));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", new Info
                {
                    Title = "Tienda Core API",
                    Description = "Todos los derechos reservados 2019",
                    Version = "v1.0"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "Por favor ingrese 'bearer {token}'", Name = "Authorization", Type = "apiKey" });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                { "Bearer", Enumerable.Empty<string>() },
            });

                //Documento de los comentarios
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                //c.DocumentFilter<TestFilter>();
            });
            //Filtros de tipos consumo y salida 
            services.AddMvc(options =>
            {
                options.Filters.Add(new ProducesAttribute("application/json", "application/xml"));
            });

            // validaciones
            services.AddMvc().AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>());
        }
      

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseAuthentication();

            app.UseCors("MyPolicy");
            //archivos estaticos
            app.UseStaticFiles();
            app.UseMvc(o =>
            {
                o.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");


            });
            //pagina por defecto
            app.UseDefaultFiles(new DefaultFilesOptions
            {
                DefaultFileNames = new List<string> { "index.html" }
            });

            if (env.IsDevelopment())
            {
                //Documentación
                app.UseSwagger(o =>
                {
                    o.RouteTemplate = "{documentName}/docs.json";


                });

            }

            app.UseSwaggerUI(o =>
            {
                o.SwaggerEndpoint("v1.0/docs.json", "Core API v1.0");
                o.DocumentTitle = "Tienda API 1.0";
                o.RoutePrefix = "";
                o.InjectStylesheet("custom.css");
                o.InjectJavascript("/swagger-ui.js");
            });
            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "wwwroot";
            });
        }
    }
}
